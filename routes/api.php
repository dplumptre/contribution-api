<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** Auth */
Route::post('register', 'ApiAuthController@register');
Route::post('login', 'ApiAuthController@authenticate');







Route::post('customer/save', 'DataController@save');
Route::put('profile/update/{user}', 'DataController@updateProfile');
Route::put('update/save/{wallet}', 'DataController@updateSave');
Route::post('check-balance-by-date/{user}', 'DataController@checkBalanceByDate');


//Route::get('open', 'DataController@open');
//Route::get('check-balance/{user}', 'DataController@checkBalance');


Route::group(['middleware' => ['jwt.verify']], function() {

    Route::get('customers', 'DataController@customers');
    Route::get('customers/summary/{user}', 'DataController@customerSummary');
    Route::get('customers/delete/{user}', 'DataController@deleteCustomers');
    Route::post('check-trans-by-date', 'DataController@checkTransByDate');

    Route::get('profile/{user}', 'DataController@profile');
    Route::get('delete-savings/{wallet}', 'DataController@deleteSave');
    Route::get('check-balance/{user}', 'DataController@checkBalance');
    Route::get('complete-account/{user}', 'DataController@completeAccount');
    Route::post('change-password/{user}', 'ApiAuthController@changePassword');
    Route::get('user', 'ApiAuthController@getAuthenticatedUser');
    //Route::get('closed', 'DataController@closed');
});