<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('role','user')->first();
        $role_admin = Role::where('role','admin')->first();
  
  
  
  
  
  
        $user = new User();
        $user->name = "Sukanmi Olanrewaju";
        $user->email =  "admin@example.com";
        // $user->password = bcrypt('2222222');
        // $user->phone = "2222222";
        $user->password = bcrypt('111111');
        $user->phone = "07087324336";
        $user->address = "24 block ave close";
        $user->status = "savings";
        $user->role_id = $role_admin->id;
        $user->save();


        $user = new User();
        $user->name = "John Doe";
        $user->email =  str_replace(' ', '.', "John Doe")."@example.com";
        $user->password = bcrypt('password123#');
        $user->phone = "09023212312";
        $user->address = "333 association ave close";
        $user->status = "loan";
        $user->role_id = $role_user->id;
        $user->save();


        $user = new User();
        $user->name = "Sandra William";
        $user->email =  str_replace(' ', '.', "Sandra William")."@example.com";
        $user->password = bcrypt('password123#');
        $user->phone = "07009088721";
        $user->address = "999 herlem way ave close";
        $user->status = "savings";
        $user->role_id = $role_user->id;
        $user->save();






    }
}
