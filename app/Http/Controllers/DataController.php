<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DataController extends Controller
{
    public function customers() 
    {
        $users = User::where('role_id',1)->orderBy('id', 'desc')->get();
        return response()->json(compact('users'),200);

    }

    public function profile(User $user) 
    {
        return response()->json(['data'=>$user],200);
    }


    public function updateProfile(Request $request,User $user) 
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'name' => 'required',
            'address' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 400);
        }

        $user->name = $request->name;
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->save();
        return response()->json(['data'=>'updated successfully'],200);

    }

    /**
     *    Account summary
     *  */
    public function customerSummary(User $user) 
    {
       // $w = Wallet::where('user_id',$user->id)->orderBy('id', 'desc')->get();
        $w = $user->wallets()->orderBy('id', 'desc')->get();
        return response()->json(['data'=>$w],200);

    }


    public function updateSave(Request $request,Wallet $wallet) 
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'mydate' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

         $wallet->narration = $request->narration;
         $wallet->mydate =  $request->mydate;
         $wallet->amount =  $request->amount;
         $wallet->user_id = $request->user_id;
         $wallet->save();
        return response()->json(['data'=>'success'],200);
    }

    public function deleteSave(Wallet $wallet) 
    {
        $wallet->delete();
        return response()->json(['data'=>'deleted successfully'],200);
    }




    /**
     *    insert money for the day
     *  */
    public function save(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'mydate' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }


        
         $w = new Wallet();
         $w->mydate = $request->mydate;
         $w->narration = $request->get('narration');
         $w->amount =  $request->get('amount');
         $w->user_id = $request->get('user_id');
         $w->save();
        return response()->json(['data'=>'success'],200);
    }

    public function deleteCustomers(User $user) 
    {
        $user->delete();
        return response()->json(['data'=>'deleted successfully'],200);
    }


    public function checkBalance(Request $request,User $user) 
    {

        $deposit=  $user->wallets()->where('status',null)->where('narration','deposit')->get('amount');
        $withdrawal=  $user->wallets()->where('status',null)->where('narration','withdrawal')->get('amount');

        // $amount_deposited = [];
        $sum=0;
        foreach($deposit as $value) {
             $sum += $value->amount;
        }

        $sbr=0;
        foreach($withdrawal as $value) {
             $sbr += $value->amount;
        }
  
        $bal = $sum - $sbr;

        //'sum'=>$sum,'expenses'=>$sbr,

        return response()->json(['bal'=>$bal ],200);  // no need to add get here
    }



    public function completeAccount(User $user) 
    {
        $complete=  $user->wallets()->update(['status'=>"completed"]);
        return response()->json(['data'=>$complete],200);  
    }



    public function checkBalanceByDate(Request $request,User $user) 
    {
        if($request->from == "" || $request->to == ""){
            return response()->json(['error'=>'from and to date required!'], 400);
        }
        $checkDeposit=  $user->wallets->where('status',null)->where('narration','deposit')->whereBetween('mydate', [$request->from,$request->to]);
        $checkWithdrawal=  $user->wallets->where('status',null)->where('narration','withdrawal')->whereBetween('mydate', [$request->from,$request->to]);
        return response()->json(['dataDeposit'=>$checkDeposit,'dataWithdrawal'=>$checkWithdrawal],200);  // no need to add get here
    }

    public function checkTransByDate(Request $request) 
    {
        if($request->from == "" || $request->to == ""){
            return response()->json(['error'=>'from and to date required!'], 400);
        }
        $deposit=  Wallet::where('status',null)
                ->where('narration','deposit')
                ->whereBetween('mydate', [$request->from,$request->to])
                ->get();
        $withdrawal=  Wallet::where('status',null)
                ->where('narration','withdrawal')
                ->whereBetween('mydate', [$request->from,$request->to])
                ->get();


                $sum=0;
                foreach($deposit as $value) {
                     $sum += $value->amount;
                }
        
                $sbr=0;
                foreach($withdrawal as $value) {
                     $sbr += $value->amount;
                }
          
                $bal = $sum - $sbr;
        


        return response()->json(['deposit'=>$sum,'withdrawal'=>$sbr,'bal'=>$bal],200);  
    }




  // no need to add get here



    // public function open() 
    // {
    //     $check= Wallet::whereBetween('mydate', ['2020-07-02','2020-07-03'])->get();
    //     return response()->json(['data'=>$check],200);
    // }



    public function closed() 
    {
        $data = "Only authorized users can see this";
        return response()->json(compact('data'),200);
    }





}
