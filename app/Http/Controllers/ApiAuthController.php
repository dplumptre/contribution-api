<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Role;



class ApiAuthController extends Controller
{

    /**
     * 
     * LOGIN WITH PHONE AND NUMBER
     *  */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('phone', 'password');
        try { 
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        
        $user = JWTAuth::user();

        return response()->json([
            'token' => $token,
            'userId' => $user->id,
            'type'=>'bearer',
            'expires'=> JWTAuth::factory()->getTTL()*60,
        ]);


    }




    public function register(Request $request)
    {
        $role_user = Role::where('role','user')->first();
      //  return response()->json(['result'=>$role_user->id]);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
           // 'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:6',
            'phone'=> 'required|numeric',
            'address' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()], 400);
        }




        $email = str_replace(" ", ".",  strtolower($request->name))."@example.com";
     
        //return response()->json(compact('email'),201);
         $user = new User();
         $user->name = $request->name;
         $user->email =  $email;
         $user->password = Hash::make('G!a7][-2ad');
         $user->phone = $request->get('phone');
         $user->address = $request->get('address');
         $user->status  = $request->get('status');
         $user->role_id = $role_user->id;
         $user->save();

         $token = JWTAuth::fromUser($user);
         return response()->json(compact('user','token'),201);
    }



    public function changePassword(Request $request,User $user)
    {
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['success'=>'Password updated successfully'],201);
    }







   public function getAuthenticatedUser()
  {
        try {

                if (! $user = JWTAuth::parseToken()->authenticate()) {
                        return response()->json(['user_not_found'], 404);
                }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }




}
